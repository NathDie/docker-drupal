<?php
/**
 * @file
 * Wodby environment configuration for generic PHP project.
 */



$wodby['files_dir'] = '/mnt/files';

$wodby['db']['host'] = 'mariadb';
$wodby['db']['name'] = 'drupal';
$wodby['db']['username'] = 'drupal';
$wodby['db']['password'] = 'drupal';
$wodby['db']['driver'] = 'mysql';

$wodby['redis']['host'] = '';
$wodby['redis']['port'] = '6379';
$wodby['redis']['password'] = '';

$wodby['varnish']['host'] = '';
$wodby['varnish']['terminal_port'] = '6082';
$wodby['varnish']['secret'] = '';
$wodby['varnish']['version'] = '';

$wodby['memcached']['host'] = '';
$wodby['memcached']['port'] = '11211';
