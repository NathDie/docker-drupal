# Docker Drupal

Création d'un docker drupal
Objectif : obtenir une instance drupal 8 utilisable en local à partir d'une stack docker

voici l'arborecence du projet :

dossier-git / 
           | - conf/ -> config wodby
           | - html/ -> instance drupal
           | - .env
           | - docker-compose.yml
